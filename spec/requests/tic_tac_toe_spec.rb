require 'spec_helper'

# This spec was generated by rspec-rails when you ran the scaffold generator.
# It demonstrates how one might use RSpec to test the controller code that
# was generated by Rails when you ran the scaffold generator.
#
# It assumes that the implementation code is generated by the rails scaffold
# generator. If you are using any extension libraries to generate different
# controller code, this generated spec may or may not pass.
#
# It only uses APIs available in rails and/or rspec-rails. There are a number
# of tools you can use to make these specs even more expressive, but we're
# sticking to rails and rspec-rails APIs to keep things simple and stable.

RSpec.describe "/games" do
  before do
    Game.delete_all
  end

  let(:app) { TicTacToe.new }

  let(:valid_attributes) {
    { player1: 'O', player2: 'X', name: 'B' }
  }
  let(:valid_turn_1) {
    { symbol: 'O', row: '0', column: '0'}
  }
  let(:repeat_player_from_turn_1) {
    { symbol: 'O', row: '0', column: '1'}
  }
  let(:used_spot_from_turn_1) {
    { symbol: 'X', row: '0', column: '0'}
  }
  let(:valid_turn_2) {
    { symbol: 'X', row: '0', column: '1'}
  }

  describe "GET /games" do
    it "renders a successful last_response" do
      Game.create! valid_attributes
      get '/games'
      expect(last_response).to be_successful
    end

    it "renders json with one game" do
      Game.create! valid_attributes
      get '/games'

      json = JSON.parse(last_response.body)
      expect(json).to be_an Array
      expect(json.size).to eq 1
    end
  end

  describe "POST /games" do
    context 'with valid attributes' do
      it "renders a successful last_response" do
        post '/games', game: valid_attributes
        expect(last_response).to be_successful
      end

      it "renders json with the game" do
        post '/games', game: valid_attributes

        json = JSON.parse(last_response.body)
        expect(json).to include valid_attributes.stringify_keys
      end
    end

    context 'with invalid attributes' do
      let(:valid_attributes) {
        { player1: 'O', player2: 'X' }
      }
      it "renders a 422" do
        post '/games', game: valid_attributes
        expect(last_response).not_to be_ok
      end

      it "renders json with errors" do
        post '/games', game: valid_attributes

        json = JSON.parse(last_response.body)
        expect(json).to include "name" => ["can't be blank"]
      end
    end
  end

  describe "GET /games/ID" do
    context 'with valid ID' do
      let!(:game) { Game.create! valid_attributes }

      it "renders a successful last_response" do
        get "/games/#{game.id}"
        expect(last_response).to be_successful
      end

      it "renders json with one game" do
        get "/games/#{game.id}"
        expect(JSON.parse(last_response.body)).to include valid_attributes.stringify_keys
      end
    end

    context 'with invalid ID' do
      let!(:game) { Game.create! valid_attributes }

      it "renders a successful last_response" do
        expect { get "/games/#{game.id+1}" }.to raise_error ActiveRecord::RecordNotFound
      end
    end
  end

  describe "DELETE /games/ID" do
    context 'with valid ID' do
      let!(:game) { Game.create! valid_attributes }

      it "renders a successful last_response" do
        delete "/games/#{game.id}"
        expect(last_response).to be_successful
      end

      it "deletes a game" do
        expect { delete "/games/#{game.id}" }.to change { Game.count }.by(-1)
      end
    end

    context 'with invalid ID' do
      let!(:game) { Game.create! valid_attributes }

      it "renders a successful last_response" do
        expect { delete "/games/#{game.id+1}" }.to raise_error ActiveRecord::RecordNotFound
      end
    end
  end

  describe "POST /games/ID/turn" do
    context 'with valid first turn' do
      let!(:game) { Game.create! valid_attributes }

      it "plays nicely" do
        post "/games/#{game.id}/turn", turn: valid_turn_1
        expect(last_response).to be_successful
        post "/games/#{game.id}/turn", turn: valid_turn_2
        expect(last_response).to be_successful
      end

      it "doesn't allow playing twice" do
        post "/games/#{game.id}/turn", turn: valid_turn_1
        expect(last_response).to be_successful
        post "/games/#{game.id}/turn", turn: repeat_player_from_turn_1
        expect(last_response).to_not be_successful
      end

      it "doesn't allow to play on used spot" do
        post "/games/#{game.id}/turn", turn: valid_turn_1
        expect(last_response).to be_successful
        post "/games/#{game.id}/turn", turn: used_spot_from_turn_1
        expect(last_response).to_not be_successful
      end
    end
  end
end
