
class CreateGamesMigration < ActiveRecord::Migration[6.1]
  def change
    create_table :games do |t|
      t.string :name, null: false, index: true
      t.string :state, index: true, null: false, default: 'waiting_for_player1'
      t.string :player1, null: false
      t.string :player2, null: false
      t.integer :turn, null: false, default: 0
      t.text :board

      t.timestamps
    end
  end
end
