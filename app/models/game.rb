class Game < ActiveRecord::Base
  PLAYER_MARKS = %w(X O)
  BOARD_SIZE = 15

  serialize :board, Board, default: Board.new(size: BOARD_SIZE)

  validates :name, presence: true, uniqueness: true
  validates :player1, presence: true, inclusion: { in: PLAYER_MARKS }
  validates :player2, presence: true, inclusion: { in: PLAYER_MARKS }
  validates :board, presence: true
  validates :state, presence: true
  validate :players_must_differ

  state_machine :state, :initial => :waiting_for_player1 do
    transition :waiting_for_player1 => :waiting_for_player2, on: :player1_move
    transition :waiting_for_player2 => :waiting_for_player1, on: :player2_move

    transition :waiting_for_player1 => :player1_won, on: :player1_wins
    transition :waiting_for_player2 => :player2_won, on: :player2_wins

    transition [:waiting_for_player1, :waiting_for_player2] => :finished_draw, on: :draw
  end

  before_validation do
    self.player1&.upcase!
    self.player2&.upcase!
  end

  # TODO: better errors
  def make_turn(symbol:, row:, column:)
    symbol = symbol.to_s.upcase
    return false if row.nil? || column.nil?
    return false unless public_send("can_player#{player_with_symbol(symbol)}_move?")
    return false unless board.valid_turn?(row, column)

    board[row, column] = symbol
    self.turn += 1


    if turn == BOARD_SIZE**2 # board.draw?
      draw
    elsif board.winner?(row, column)
      public_send("player#{player_with_symbol(symbol)}_wins")
    else
      public_send("player#{player_with_symbol(symbol)}_move", row, column)
    end

    save
  end

  def player_with_symbol(symbol)
    case symbol.to_s.upcase
    when player1 then 1
    when player2 then 2
    else nil
    end
  end

  private

  def players_must_differ
    if player1 == player2
      errors.add(:player1, "must not be the same")
    end
  end
end
