require 'matrix'

# Zero based board
class Board
  WIN_CONDITION = 5
  attr_reader :matrix

  def initialize(size: 15, matrix: nil)
    @matrix = Matrix.build(size, size) { nil }
  end

  def []=(row, column, value)
    @matrix[row, column] = value
  end

  def [](row, column)
    @matrix[row, column]
  end

  # TODO: implement without knowing last turn
  # TODO: performance will be better without comparing win_sequence as string, but array
  def winner?(last_row, last_column)
    last_symbol = @matrix[last_row, last_column]
    return false if last_symbol.nil?

    win_sequence = last_symbol * WIN_CONDITION

    # check rows
    lowest_possible_column = [last_column-WIN_CONDITION, 0].max
    return true if @matrix.row(last_row)[lowest_possible_column .. last_column+5].join('').include? win_sequence

    # check columns
    lowest_possible_row = [last_row-WIN_CONDITION, 0].max
    return true if @matrix.column(last_column)[lowest_possible_row .. last_column+5].join('').include? win_sequence

    # check diagonals
    diagonal1 = (-WIN_CONDITION .. WIN_CONDITION).map do |i|
      @matrix[lowest_possible_row + i, lowest_possible_column + i]
    end
    return true if diagonal1.join('').include?(win_sequence)

    diagonal2 = (-WIN_CONDITION .. WIN_CONDITION).map do |i|
      @matrix[lowest_possible_row + WIN_CONDITION * 2 - i, lowest_possible_column + i]
    end
    return true if diagonal2.join('').include?(win_sequence)

    false
  end

  def draw?
    !@matrix.any?(nil)
  end

  def valid_turn?(row, column)
    return false if row > @matrix.row_count - 1 || row < 0
    return false if column > @matrix.column_count - 1 || column < 0

    @matrix[row, column].nil?
  end

  def as_json
    { size: @matrix.row_count, matrix: @matrix.as_json }
  end
end
