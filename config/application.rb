require 'active_record'
require 'state_machines'
require 'state_machines-activerecord'
require 'erb'

require_relative '../app/models/board'
require_relative '../app/models/game'

config = YAML.load(ERB.new(File.read('db/config.yml')).result(binding))[ENV['RACK_ENV'] || 'development']

ActiveRecord::Base.establish_connection(config)
