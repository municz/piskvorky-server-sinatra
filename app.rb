require_relative 'config/application'
require 'sinatra'

class TicTacToe < Sinatra::Base
  get '/games' do
    Game.all.to_json
  end

  get '/games/:id' do
    Game.find(params[:id]).to_json
  end

  post '/games' do
    @game = Game.new(params[:game].to_h.slice(:name, :player1, :player2))
    if @game.save
      @game.to_json
    else
      status 422
      @game.errors.to_json
    end
  end

  post '/games/:id/turn' do
    @game = Game.find(params[:id])

    turn_params = params[:turn].to_h.symbolize_keys
    turn_params[:row] = Integer(turn_params[:row], exception: false)
    turn_params[:column] = Integer(turn_params[:column], exception: false)

    if @game.make_turn(**turn_params)
      @game.to_json
    else
      status 422
      @game.errors.to_json
    end
  end

  delete '/games/:id' do
    Game.find(params[:id]).destroy
  end
end
